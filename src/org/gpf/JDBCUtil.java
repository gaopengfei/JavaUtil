package org.gpf;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class JDBCUtil {

	private JDBCUtil(){
		
	}
	
	/**
	 * 通过属性文件jdbc.properties得到数据连接
	 */
	public static Connection getConnection() throws Exception{
		
		Properties properties = new Properties();
		InputStream in = JDBCUtil.class.getClassLoader().getResourceAsStream("jdbc.properties");
		properties.load(in);
		
		String user = properties.getProperty("jdbc.user");
		String password = properties.getProperty("jdbc.password");
		String url = properties.getProperty("jdbc.url");
		String driver = properties.getProperty("jdbc.driver");
		
		/**
		 * 载数据库驱动程序(对应的 Driver 实现类中有注册驱动的静态代码块.)
		 */
		Class.forName(driver);
		
		/**
		 * DriverManager 是驱动的管理类. 
		 * 1). 可以通过重载的 getConnection() 方法获取数据库连接. 较为方便
		 * 2). 可以同时管理多个驱动程序: 若注册了多个数据库连接, 则调用 getConnection()
		 * 方法时传入的参数不同, 即返回不同的数据库连接
		 */
		return DriverManager.getConnection(url, user, password);
	}
	
	/**
	 * 释放数据库资源
	 * @param resultSet
	 * @param statement
	 * @param connection
	 */
	public static void release(ResultSet resultSet, Statement statement,
			Connection connection) {

		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 根据SQL语句执行更新操作（增加、删除、修改）
	 * @param sql：sql可以是INSERT、DELETE、UPDATE
	 */
	public static void update(String sql) {
		Connection connection = null;
		Statement statement = null;

		try {
			connection = getConnection();
			statement = connection.createStatement();
			statement.executeUpdate(sql);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			release(null, statement, connection);
		}
	}
	
	/**
	 * 传入带有占位符的SQL语句和SQL语句的参数进行更新操作
	 * @param sql：带有参数的SQL语句
	 * @param args：SQL语句的参数
	 */
	public static void update(String sql, Object ... args){
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(sql);
			for(int i = 0; i < args.length; i++){
				preparedStatement.setObject(i + 1, args[i]);
			}
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			release(null, preparedStatement, connection);
		}
	}
}
