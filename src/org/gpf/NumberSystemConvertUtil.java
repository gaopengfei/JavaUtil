package org.gpf;


/**
 * 进制转换工具类，实现二进制、八进制、十六进制、十进制整数的互转
 * 
 * @author gaopengfei
 * @date 2015-4-15 上午8:43:08
 */
public class NumberSystemConvertUtil {
	
	/**
	 * 将工具类的构造私有化，防止其产生实例化对象
	 */
	private NumberSystemConvertUtil() {
		
	}

	/**
	 * 十进制转化为二进制字符串，因为是位操作，所以支持负数
	 * @param number
	 * @return
	 */
	public static String dec2Bin(int number) {

		return convert(number, 1, 1);
	}

	/**
	 * 十进制转化为8进制字符串
	 * @param number
	 * @return
	 */
	public static String dec2Oct(int number) {

		return convert(number, 07, 3);
	}

	/**
	 * 十进制转化为16进制字符串
	 * 
	 * @param number
	 * @return
	 */
	public static String dec2Hex(int number) {

		return convert(number, 0xF, 4);
	}
	
	/**
	 * 通用的转换方法
	 * @param number 需要转换的数
	 * @param base 表示是多少进制
	 * @param offset 应该移位的位数	
	 * @return
	 */
	public static String convert(int number, int base, int offset) {

		StringBuilder sb = new StringBuilder();

		while (number != 0) {
			sb.insert(0, number2HexCharacter(number & base));
			number >>>= offset; // 此处为无符号右移
		}

		return sb.toString();
	}

	/**
	 * 0~15的整数可以转化为其对应的16进制字符
	 * 还有一种思路：声明一个数组，保存0~F这16个数，根据下标取得对应字符【查表法】
	 * @param number
	 * @return
	 */
	public static char number2HexCharacter(int number) {

		char[] dictionary = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
		if (number >= 0 && number < 16) {
			return dictionary[number];
		}else {
			return '\0';
		}
		
		/*
		if (number >= 0 && number < 16) {
			if (number < 10)
				return (char) ('0' + number);
			else
				return (char) (number - 10 + 'A');
		} else {
			return '\0';
		}
		*/
	}

}
