package org.gpf;

/**
 * 查找工具类，实现简单的查找、二分查找
 * 
 * @author gaopengfei
 * @date 2015-4-14 上午9:22:23
 */
public class FindUtil {
	
	/**
	 * 构造方法私有化
	 */
	private FindUtil() {
		
	}

	/**
	 * 普通查找
	 * @param arr数组
	 * @param key需要查找的关键字
	 * @return
	 */
	public static int search(int arr[], int key) {

		if (arr == null || arr.length == 0) {
			return -1;
		}
		for (int i = 0; i < arr.length; i++) {
			if (key == arr[i])
				return i;
		}
		return -1;
	}

	/**
	 * 二分查找的非递归实现 特别注意，以下算法只能针对一个升序排列的数组，返回key所在的索引或者该key应该插入的位置的负值
	 */
	public static int binarySearch(int[] arr, int key) {

		if (arr == null || arr.length == 0){ 
			return -1;
		}
		
		int minIndex = 0, maxIndex = arr.length - 1, midIndex;
		while (minIndex <= maxIndex) {
			midIndex = (minIndex + maxIndex) >> 1; 		// 右移一位相当于除以2
			if (key == arr[midIndex])
				return midIndex;
			if (key < arr[midIndex])
				maxIndex = midIndex - 1;
			if (key > arr[midIndex])
				minIndex = midIndex + 1;
		}
		return -minIndex;								// 如果最低的索引比最高索引高就表示没有找到,minIndex即为应该插入的位置
	}

	/**
	 * 二分查找的递归实现---实际上是一种分治算法
	 * 
	 * @param arr数组
	 * @param midIndex最低位置
	 * @param maxIndex最高位置
	 * @param key需要查找的值
	 * @return元素的位置
	 */
	public static int binarySearch(int[] arr, int minIndex, int maxIndex,
			int key) {

		if (arr == null || arr.length == 0) 
			return -1;
		if (minIndex <= maxIndex) {
			int midIndex = (minIndex + maxIndex) / 2;
			if (key == arr[midIndex])
				return midIndex;
			else if (key < arr[midIndex])
				return binarySearch(arr, minIndex, midIndex - 1, key);
			else
				return binarySearch(arr, midIndex + 1, maxIndex, key);
		} else {
			return -minIndex;
		}

	}
	
}
