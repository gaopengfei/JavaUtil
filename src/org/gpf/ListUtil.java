package org.gpf;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * List工具类，可以完成去除列表中的重复元素
 * 
 * @author gaopengfei
 * @date 2015年6月5日 下午7:47:31
 */
public class ListUtil {

	private ListUtil() {

	}

	/**
	 * 去除列表中的重复元素
	 * 
	 * @param list
	 * @return
	 */
	public static <E> List<E> singleElement(List<E> list) {

		List<E> tmp = new ArrayList<E>();
		Iterator<E> iterator = list.iterator();
		while (iterator.hasNext()) {
			E e = iterator.next();
			if (!tmp.contains(e)) {
				tmp.add(e);
			}
		}

		return tmp;
	}

}
